# Plugin Gallery Slider for WP

Plugin para mostrar Galería Slider en WordPress con parámetros configurables.
Genera un Shortcode para colocarlo en cualquier sitio de la web.

Repositorio privado https://gitlab.com/diurvan/diu-gallery-slider-wp


Puede consultarnos en https://wa.me/+51955478664 o a ivan.tapia@diurvanconsultores.com
